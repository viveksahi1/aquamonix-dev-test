import requests
import base64
from datetime import datetime
import json
import csv
import sys
import time

#Base URL
URL = "https://api.waterwatch.io/v1"

#Generate Auth Token
api_key = "MGQxOGUxZWMtM2Q2Ny00NjNlLTg1MzMtZWUyZWY5ODNiN2Vk"
api_token = "MTUzMzUyNjI2ODoxODQ4ODg2MjY4OmJjZTJjZjM4LTM4OGMtNDZmZi04ZGJjLTM4ZTUzZmRiYTEyNA=="

headeValue = api_key + ":" + api_token
encodedHeaderValue  = base64.b64encode(bytes(headeValue, "utf-8"))

"""
    Convert given date (dd/mm/yyyy) to unix time
"""
def get_unix_ms(date_time):
    try:
        dt_object1 = datetime.strptime(date_time, "%d/%m/%Y")
        return int(dt_object1.timestamp() * 1000)
         
    except ValueError:
        print ("Error in Date Time format. Expecting in format '04/09/2019'")
        return None

"""
    Convert unix time (ms) to readable timestamp
"""
def get_timestamp(time_ms):
    return datetime.fromtimestamp(
                int(time_ms)/1000).strftime('%Y-%m-%d %H:%M:%S')

"""
    Convert retrieved sensor data in appropiate format
"""
def write_to_csv(filename, api_response):
    m_list = []
    currentReading = 0

    #handle csv file writing
    f = open(filename + ".csv", "w", newline='')
    writer = csv.writer(f, quoting=csv.QUOTE_NONNUMERIC)

    measurements = json.loads(api_response.text)

    #logic for getting reading every hour
    for m in measurements['measurements']:
        if (currentReading == 0) or ( (int(m["time"]) - currentReading) >= 3600000): #360000 =  ms in hr
            currentReading = int(m["time"])
            timeStamp = get_timestamp(m["time"])
            m_list.append((timeStamp, m["rawDistance"]))

    writer.writerows(m_list)
    f.close()
    print("CSV file generated Succesfully")

    
#Log Sensor from this time
while  True:
    startTime = get_unix_ms(input("Enter Date in format DD/MM/YYYY: "))
    if (startTime is not None):
        break
    
#startTime = get_unix_ms(dateStart) 
endTime = startTime + 86400000 # 24hrs of data
  
# BUILD Request
response = requests.get(URL + '/organisations/28206280-9929-11e8-8c5d-b160a11fab36/sensors/419042/measurements'
                   +'?startTime='+ str(startTime)
                   + '&endTime='+ str(endTime)
                   + '&order=asc',
                   headers={'Authorization': encodedHeaderValue})

write_to_csv("output", response)

#wait to display message before exiting shell
print("Exiting!")
time.sleep(2)

