<?php

// Open CSV file and load into array
$handle = fopen("output.csv", "r");
while (($data[] = fgetcsv($handle, 1000, ",")) !== FALSE);
fclose($handle);

// load sensor data into array
$chart_data = array();
for ($i = 0; $i < count($data); $i++) {
    array_push($chart_data, array("y" => $data[$i][1], "label" => $data[$i][0]));
}

?>

<!DOCTYPE HTML>
<html>

<head>
    <title> Aquamonix dev test</title>

    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/default.css">

    <!-- Handle javascript for displaying chart -->
    <script>
        window.onload = function() {

            var chart = new CanvasJS.Chart("chartContainer", {
                title: {
                    text: "Water Watch Sensor data"
                },
                axisY: {
                    title: "Raw Sensor Data values",
                    includeZero: false
                },
                data: [{
                    type: "stepLine",
                    dataPoints: <?php echo json_encode($chart_data, JSON_NUMERIC_CHECK); ?>
                }]
            });
            chart.render();

        }
    </script>
</head>

<body>
    <h1 class="top-heading"> Aquamonix Developer Test</h1>
    <div class="container">
        <div class="row">
            <div id="chartContainer" style="height: 300px; width: 100%;"></div>
        </div>
    </div>

    <script src="js/canvasjs.min.js"></script>
</body>

</html>